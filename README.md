Digital Electronic Circuits
===========================

Slides, examples and assignments for the course "Digital Electronic Circuits"
of the ETSI Informática de Sevilla.

This is a personal workspace of the author and does not have to be the same
contents used in the oficial courses.

* [Browse PDF files](https://gitlab.com/jjchico-ced/ced-units/-/jobs/artifacts/main/browse?job=pdf).

Other course's repos available at [Gitlab](https://gitlab.com/jjchico-ced).

Units
-----

* [Introduction](unit-01_intro/): Introduction to the course.

* [Electronics](unit-01_zelectronics/): General introduction to electronic
  technology.

* [Combinational circuits](unit-03_combinational/): Combinational circuits
  design and analysis theory and problem solving using pen and paper.

* [Hardware Description Languages](unit-03_zhdl/): Introduction to Hardware
  Description Languages (HDLs) and Verilog.

* [Combinational subsystems](unit-04_comb_sub/): Typical combinational blocks
  (decoders, multiplexers, comparators, etc.) and how to use them to design
  digital circuits.

* [Arithmetic and logic units](unit-05_arithmetic/): Introduction to the
  design of basic arithmetic circuits and Arithmetic-and-Logic Units (ALUs).

* [Sequential circuits](unit-06_sequential/): Sequential circuits and digital
  circuit design using Finite-State Machines (FSMs).
  
* [Sequential subsystems](unit-07_seq_sub/): Typical sequential blocks
  (registers and counters) and how to use them to design digital circuits.

Author
------

Jorge Juan-Chico <jjchico@dte.us.es>

Acknowledgments
---------------

Inspired by the work of many at the Departamento de Tecnología Electrónica,
Universidad de Sevilla.

Licence
-------

CC BY-SA 4.0 [Creative Commons Atribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/)
