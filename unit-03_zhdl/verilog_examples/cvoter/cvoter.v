// Conditional voting circuit
// File: cvoter.v
//  z = ab + bc + ac if x = 1
//  z = 0 if x = 0;

`timescale 1ns / 1ps

module cvoter(
    input wire x,
    input wire a,
    input wire b,
    input wire c,
    output reg z
    );

    wire v;

    assign v = a&b | b&c | a&c;

    always @(*)
        if (x == 1)
            z = v;
        else
            z = 0;

endmodule  // cvoter
