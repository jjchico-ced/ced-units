// Conditional voter circuit. Test bench.
// File: cvoter_tb.v

`timescale 1ns / 1ps

module test ();
    wire z;
    reg a, b, c, x;

    cvoter uut (
        .a(a), .b(b), .c(c), .x(x), .z(z)
    );
    initial begin
        // Simulation results
        $dumpfile("cvoter_tb.vcd");
        $dumpvars(0, test);

        // initial input values
        a = 0;
        b = 0;
        c = 0;
        x = 0;

        // input signal changes
        #10 a = 1;
        #10 c = 1;
        #10 b = 1;
        #10 x = 1;
        #10 c = 0;
        #10 b = 0;
        #10 a = 0;
        #20 $finish;
    end
endmodule