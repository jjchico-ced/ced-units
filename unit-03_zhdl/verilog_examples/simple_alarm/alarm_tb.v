// Simple alarm circuit. Test bench.
// File: alarm_tb.v

`timescale 1ns / 1ps

module test ();

    wire alarm;
    reg on, fire, pres;

    alarm uut (.on(on), .fire(fire), .pres(pres), .alarm(alarm));

    initial begin
        // Simulation results
        $dumpfile("alarm_tb.vcd");
        $dumpvars(0, test);

        // initial input values
        on = 0;
        fire = 0;
        pres = 0;
        // input signal changes
        #10 fire = 1;
        #10 pres = 1;
        #10 fire = 0;
        #10 on = 1; pres = 0;
        #10 fire = 1;
        #10 pres = 1;
        #10 fire = 0;
        #20 $finish;
    end
endmodule
