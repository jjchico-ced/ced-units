// Simple alarm circuit.
// File: alarm.v

`timescale 1ns / 1ps

module alarm (
    input wire on,          // on switch
    input wire fire,        // fire detector (active high)
    input wire pres,        // presence detector (active low)
    output wire alarm       // alarm output
    );

    // always activate the alarm with fire
    // with presence, activate only if on
    assign alarm = fire | on & ~pres;

endmodule
