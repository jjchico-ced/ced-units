#!/bin/sh -e

paths="unit-*"
output_dir="../PDF"

mkdir -p "$output_dir"

for dir in $paths; do
    cd "$dir"
    if ls *.odp 2> /dev/null; then
         loimpress --convert-to pdf --outdir "$output_dir" *.odp
    fi
    if ls *.odg 2> /dev/null; then
         lodraw --convert-to pdf --outdir "$output_dir" *.odg
    fi
     if ls *.odt 2> /dev/null; then
         lowriter --convert-to pdf --outdir "$output_dir" *.odt
    fi

    # xopp to pdf disabled due to xournalpp on CI error
    #if ls *.xopp 2> /dev/null; then
    #    for file in `ls *.xopp`; do
    #        pdf_file=${file%.*}.pdf
    #        xournalpp --export-no-background --export-no-ruling \
    #          -p "$output_dir/$pdf_file" $file
    #    done
    #fi
    cd ..
done
