// Project:     Sequence detector analysis
// File:        Mealy's FSM design
// Description: Anaysis of a sequential circuit implementing a
//              Finite-State Machine (FSM)
// Author:      Jorge Juan-Chico <jjchico@dte.us.es>
// Date:        2019-11-27 (initial)

//// Sequence detector FSM description ////

module sequence (
    input wire clk,     // posedge clk
    input wire reset,   // active-low reset
    input wire x,       // data input
    output reg z        // data output
    );

    // state definition
    localparam [1:0] A = 0,
                     B = 1,
                     C = 2;

    // state signals
    reg [1:0] state, next_state;

    // sequential process
    always @(posedge clk, negedge reset)
        if (reset == 1'b0)
            state = A;
        else
            state <= next_state;

    // next state process
    always @(*)
        case (state)
        A:  if (x == 1'b0)          // waiting for 1st bit (1)
                next_state = A;
            else
                next_state = B;
        B:  if (x == 1'b0)          // waiting for 2nd bit (0)
                next_state = C;
            else
                next_state = B;
        C:  next_state = A;         // waiting for 3rd bit (1)
        endcase

    // output process
    always @(*)
        if (state == C && x == 1'b1)
            z = 1'b1;               // 3rd bit is '1': correct sequence
        else
            z = 1'b0;
endmodule
