// Project:     Sequence detector analysis
// File:        Test bench
// Description: Anaysis of a sequential circuit implementing a
//              Finite-State Machine (FSM)
// Author:      Jorge Juan-Chico <jjchico@dte.us.es>
// Date:        2019-11-27 (initial)

`timescale 1ns / 1ps

// Un-comment to delay the expected output one cycle (simulate Moore FSM)
//`define MOORE

// Device delay, applied to every gate and flip-flop (ns)
`ifndef DELAY
`define DELAY 10
`endif

// Clock period (ns)
`ifndef CYCLE
`define CYCLE 50
`endif

module test;

    // test sequences
    reg [0:23] seq  = 24'b001001010100011011011100;  // input sequence
    reg [0:23] eseq = 24'b000000010000000010010000;  // expected output

    integer i = 0;  // sequence index
    reg ze;         // expected output

    reg clk;        // posedge
    reg reset;      // active-low
    reg x;          // input
    wire z;         // output

    sequence #(.delay(`DELAY)) uut (.clk(clk), .reset(reset), .x(x), .z(z));

    // clock generator
    always
        #(`CYCLE/2) clk = ~clk;

    initial begin
        // waveforms
        $dumpfile("seq_tb.vcd");
        $dumpvars(0, test);

        // print header
        $display("N=seq. num, x=input, z=output, ze=expected output");
        $display(" N  x  z ze");

        // initialization
        #0;
        clk = 0;
        reset = 0;
        x = 0;
        #10 reset = 1;
    end

    // sequence generation and test
    initial begin
`ifdef MOORE
        eseq = eseq >> 1;
`endif
        while (i<24) begin
            #1 x = seq[i];
            ze = eseq[i];

            @(negedge clk);
            $display("%2d  %b  %b %b", i, x, z, ze);
            if (z != eseq[i])
                $display("ERROR: sequence num %2d: z = %b, expected %b",
                         i, z, ze);

            @(posedge clk);
            i = i + 1;
        end
        $finish;
    end

endmodule
