// Project:     Sequence detector analysis
// File:        Structural Mealy's design
// Description: Anaysis of a sequential circuit implementing a
//              Finite-State Machine (FSM)
// Author:      Jorge Juan-Chico <jjchico@dte.us.es>
// Date:        2019-11-27 (initial)

//// Sequence detector structural description (see schematic) ////

`timescale 1ns / 1ps

// JK flip-flop

module jkff #(
    parameter delay = 0
    )(
    input wire clk,     // posedge clk
    input wire cl,      // active-low asynchronous clear
    input wire j,       // set
    input wire k,       // reset
    output reg q
    );

    always @(posedge clk, negedge cl)
        if (cl == 1'b0)
            q <= 1'b0;
        else
            case ({j,k})
            2'b01:  q <= #delay 1'b0;
            2'b10:  q <= #delay 1'b1;
            2'b11:  q <= #delay ~q;
            endcase
endmodule

// Main module

module sequence #(
    parameter delay = 0
    )(
    input wire clk,     // posedge clk
    input wire reset,   // active-low reset
    input wire x,       // data input
    output wire z       // data output
    );

    // internal wires
    wire x_neg, j1, q1, q2;

    not #delay not1 (x_neg, x);
    and #delay and1 (j1, x_neg, q2);
    jkff #(.delay(delay))
        jkff1 (.clk(clk), .cl(reset), .j(j1), .k(1'b1), .q(q1));
    jkff #(.delay(delay))
        jkff2 (.clk(clk), .cl(reset), .j(x), .k(q1), .q(q2));
    and #delay and2 (z, x, q1);
endmodule
